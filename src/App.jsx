import React, { Component } from "react";
//import ReactDOM from "react-dom";
import * as THREE from "three";
import {OrbitControls} from "three/examples/jsm/controls/OrbitControls"

class App extends Component {
      componentDidMount() {

        let scene, cube, camera, renderer, controls

        const init = () => {
          scene = new THREE.Scene();
          scene.background = new THREE.CubeTextureLoader()
          .setPath("https://raw.githubusercontent.com/Daefher/RetroScene/master/assets/Skybox/")
          .load([
            "skybox_right.png",
            "skybox_left.png",
            "skybox_up.png",
            "skybox_down.png",
            "skybox_back.png",
            "skybox_front.png"
          ])
  
          camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
          camera.position.z = 25
          camera.position.y = 15
          
          renderer = new THREE.WebGLRenderer({antialias: true});
          renderer.setSize(window.innerWidth, window.innerHeight);
          renderer.setPixelRatio(window.devicePixelRatio)
          document.body.appendChild(renderer.domElement);
  
          let axesHelper = new THREE.AxesHelper()
          scene.add(axesHelper)
          axesHelper.position.y = 5
          
          controls = new OrbitControls(camera, renderer.domElement)
  
          let light = new THREE.DirectionalLight(0xffffff)
          light.position.set(0, 10, 1)
          scene.add(light)
  
          let hemi = new THREE.HemisphereLight(0xffffff, 0xffffff, 0.5)
          hemi.position.set(0, 0, 5)
          scene.add(hemi)
          
          let planeGeometry = new THREE.PlaneGeometry(1000, 1000, 30, 30)
          planeGeometry.rotateX(-Math.PI / 2)
          //planeGeometry.position.set(0, -10, 0)
          let planeMaterial = new THREE.MeshLambertMaterial({color: 0x00ff00, wireframe: true})
          let plane = new THREE.Mesh(planeGeometry, planeMaterial)
          scene.add(plane)
          
          let geometry = new THREE.BoxGeometry(3, 3, 3);
          let cubeMaterials = [
            new THREE.MeshBasicMaterial({ map: new THREE.TextureLoader().load("https://raw.githubusercontent.com/chrisdavidmills/threejs-video-cube/gh-pages/textures/metal003.png"), side: THREE.DoubleSide }),
            new THREE.MeshBasicMaterial({ map: new THREE.TextureLoader().load("https://raw.githubusercontent.com/chrisdavidmills/threejs-video-cube/gh-pages/textures/metal003.png"), side: THREE.DoubleSide }),
            new THREE.MeshBasicMaterial({ map: new THREE.TextureLoader().load("https://raw.githubusercontent.com/chrisdavidmills/threejs-video-cube/gh-pages/textures/metal003.png"), side: THREE.DoubleSide }),
            new THREE.MeshBasicMaterial({ map: new THREE.TextureLoader().load("https://raw.githubusercontent.com/chrisdavidmills/threejs-video-cube/gh-pages/textures/metal003.png"), side: THREE.DoubleSide }),
            new THREE.MeshBasicMaterial({ map: new THREE.TextureLoader().load("https://raw.githubusercontent.com/chrisdavidmills/threejs-video-cube/gh-pages/textures/metal003.png"), side: THREE.DoubleSide }),
            new THREE.MeshBasicMaterial({ map: new THREE.TextureLoader().load("https://raw.githubusercontent.com/chrisdavidmills/threejs-video-cube/gh-pages/textures/metal003.png"), side: THREE.DoubleSide }),
          ]
          let material = new THREE.MeshFaceMaterial(cubeMaterials);
          cube = new THREE.Mesh(geometry, material);
          scene.add(cube);
          cube.position.y = 10
          camera.lookAt(0, 25, 15)
        }

        const animate = () => {
          requestAnimationFrame(animate);
          cube.rotation.x += 0.01;
          cube.rotation.y += 0.01;
          camera.position.z -= 0.01
          camera.position.x += 0.01

          renderer.render(scene, camera);
          controls.update()
        };

        window.addEventListener("resize", () => {
          renderer.setSize(window.innerWidth, window.innerHeight);
          renderer.setPixelRatio(window.devicePixelRatio)
          renderer.render(scene, camera)
        })

        init()
        animate();
      }
  render() {
    return (
      <div />
    )
  }
}

//const rootElement = document.getElementById("root");
//ReactDOM.render(<App />, rootElement);

export default App;
