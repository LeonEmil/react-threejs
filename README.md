# Demo project with react and threejs

This project is a demo of threejs in react using basic geometry, lights and controls in a componentDidMount()

See the demo here:
[https://leonemil.gitlab.io/react-threejs/](https://leonemil.gitlab.io/react-threejs/)

![Demostración de threejs con React. En la imagen se ve un cubo sobre un plano tridimensional](https://res.cloudinary.com/leonemil/image/upload/v1595861873/Demos/threejs-demo.jpg)